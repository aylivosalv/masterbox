# MasterBoxFreelanceExchange.InlineResponse2002

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Наименование города | [optional] 
**code** | **Number** | Код города | [optional] 


