# MasterBoxFreelanceExchange.DefaultApi

All URIs are relative to *https://virtserver.swaggerhub.com/aylivosalv/MasterBox/0.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**categoriesGet**](DefaultApi.md#categoriesGet) | **GET** /categories | 
[**citiesGet**](DefaultApi.md#citiesGet) | **GET** /cities | 
[**executorIdAddphotoPost**](DefaultApi.md#executorIdAddphotoPost) | **POST** /executor/{id}/addphoto | 
[**executorIdDelete**](DefaultApi.md#executorIdDelete) | **DELETE** /executor/{id} | 
[**executorIdGet**](DefaultApi.md#executorIdGet) | **GET** /executor/{id} | 
[**executorIdOffersGet**](DefaultApi.md#executorIdOffersGet) | **GET** /executor/{id}/offers | 
[**executorIdOrdersGet**](DefaultApi.md#executorIdOrdersGet) | **GET** /executor/{id}/orders | 
[**executorIdPut**](DefaultApi.md#executorIdPut) | **PUT** /executor/{id} | 
[**executorIdSettingsEduPatch**](DefaultApi.md#executorIdSettingsEduPatch) | **PATCH** /executor/{id}/settings/edu | 
[**executorIdSettingsEmailPatch**](DefaultApi.md#executorIdSettingsEmailPatch) | **PATCH** /executor/{id}/settings/email | 
[**executorIdSettingsNamePatch**](DefaultApi.md#executorIdSettingsNamePatch) | **PATCH** /executor/{id}/settings/name | 
[**executorIdSettingsPasswdPatch**](DefaultApi.md#executorIdSettingsPasswdPatch) | **PATCH** /executor/{id}/settings/passwd | 
[**executorIdSettingsPhonePatch**](DefaultApi.md#executorIdSettingsPhonePatch) | **PATCH** /executor/{id}/settings/phone | 
[**executorIdSettingsSkillsPatch**](DefaultApi.md#executorIdSettingsSkillsPatch) | **PATCH** /executor/{id}/settings/skills | 
[**executorIdStatOrderstatGet**](DefaultApi.md#executorIdStatOrderstatGet) | **GET** /executor/{id}/stat/orderstat | 
[**executorIdStatRatingstatGet**](DefaultApi.md#executorIdStatRatingstatGet) | **GET** /executor/{id}/stat/ratingstat | 
[**executorPost**](DefaultApi.md#executorPost) | **POST** /executor | 
[**messagesIdGet**](DefaultApi.md#messagesIdGet) | **GET** /messages/{id} | Получение сообщения по id
[**offersIdDelete**](DefaultApi.md#offersIdDelete) | **DELETE** /offers/{id} | 
[**offersIdGet**](DefaultApi.md#offersIdGet) | **GET** /offers/{id} | 
[**offersIdPut**](DefaultApi.md#offersIdPut) | **PUT** /offers/{id} | 
[**offersPost**](DefaultApi.md#offersPost) | **POST** /offers | 
[**orderIdRespondsGet**](DefaultApi.md#orderIdRespondsGet) | **GET** /order/{id}/responds/ | Получить отклики по id заказа
[**orderIdRespondsPost**](DefaultApi.md#orderIdRespondsPost) | **POST** /order/{id}/responds/ | Добавить отклик в заказ по id
[**ordersGet**](DefaultApi.md#ordersGet) | **GET** /orders/ | Получение списка заказов заказчика
[**ordersIdDelete**](DefaultApi.md#ordersIdDelete) | **DELETE** /orders/{id} | Изменение заказа
[**ordersIdFeedbacksGet**](DefaultApi.md#ordersIdFeedbacksGet) | **GET** /orders/{id}/feedbacks/ | Отзывы заказчика и исполнителя к заказу
[**ordersIdGet**](DefaultApi.md#ordersIdGet) | **GET** /orders/{id} | Получение подробного заказа заказчика
[**ordersIdMessagesDelete**](DefaultApi.md#ordersIdMessagesDelete) | **DELETE** /orders/{id}/messages/ | Удалить сообщения, связанные с заказом
[**ordersIdMessagesGet**](DefaultApi.md#ordersIdMessagesGet) | **GET** /orders/{id}/messages/ | Получение сообщений заказа
[**ordersIdMessagesPost**](DefaultApi.md#ordersIdMessagesPost) | **POST** /orders/{id}/messages/ | Добавить сообщение к заказу
[**ordersIdPut**](DefaultApi.md#ordersIdPut) | **PUT** /orders/{id} | Изменение заказа
[**ordersPost**](DefaultApi.md#ordersPost) | **POST** /orders/ | Добавление заказа
[**respondsIdDelete**](DefaultApi.md#respondsIdDelete) | **DELETE** /responds/{id} | Получить отклик по id
[**respondsIdGet**](DefaultApi.md#respondsIdGet) | **GET** /responds/{id} | Получить отклик по id
[**respondsIdPut**](DefaultApi.md#respondsIdPut) | **PUT** /responds/{id} | Изменить отклик по id
[**searchOffersGet**](DefaultApi.md#searchOffersGet) | **GET** /search/offers | 
[**searchOrdersGet**](DefaultApi.md#searchOrdersGet) | **GET** /search/orders | 
[**usersCustomersGet**](DefaultApi.md#usersCustomersGet) | **GET** /users/customers/ | Получение всех пользователей-заказчиков
[**usersCustomersPost**](DefaultApi.md#usersCustomersPost) | **POST** /users/customers/ | Добавление нового пользователя как заказчика
[**usersIdDelete**](DefaultApi.md#usersIdDelete) | **DELETE** /users/{id} | Удаление пользователя
[**usersIdFeedbacksCustomerPost**](DefaultApi.md#usersIdFeedbacksCustomerPost) | **POST** /users/{id}/feedbacks/customer/ | Добавить отзыв от лица заказчика
[**usersIdFeedbacksWorkerPost**](DefaultApi.md#usersIdFeedbacksWorkerPost) | **POST** /users/{id}/feedbacks/worker/ | Добавить отзыв от лица исполнителя
[**usersIdGet**](DefaultApi.md#usersIdGet) | **GET** /users/{id} | Получение пользователя
[**usersIdMessagesGet**](DefaultApi.md#usersIdMessagesGet) | **GET** /users/{id}/messages/ | Получение сообщений пользователя
[**usersIdSettingSetAvatarPatch**](DefaultApi.md#usersIdSettingSetAvatarPatch) | **PATCH** /users/{id}/setting/set/avatar | 
[**usersIdSettingSetEmailPatch**](DefaultApi.md#usersIdSettingSetEmailPatch) | **PATCH** /users/{id}/setting/set/email | 
[**usersIdSettingSetNameSurnamePatch**](DefaultApi.md#usersIdSettingSetNameSurnamePatch) | **PATCH** /users/{id}/setting/set/name_surname | 
[**usersIdSettingSetPasswordPatch**](DefaultApi.md#usersIdSettingSetPasswordPatch) | **PATCH** /users/{id}/setting/set/password | 
[**usersIdSettingSetPhonePatch**](DefaultApi.md#usersIdSettingSetPhonePatch) | **PATCH** /users/{id}/setting/set/phone | 


<a name="categoriesGet"></a>
# **categoriesGet**
> [InlineResponse2003] categoriesGet()



Получение списка категорий заказов

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.categoriesGet(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[InlineResponse2003]**](InlineResponse2003.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="citiesGet"></a>
# **citiesGet**
> [InlineResponse2002] citiesGet()



Получение списков городов

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.citiesGet(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[InlineResponse2002]**](InlineResponse2002.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="executorIdAddphotoPost"></a>
# **executorIdAddphotoPost**
> executorIdAddphotoPost(id, photo)



### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | 

var photo = new MasterBoxFreelanceExchange.Photo(); // Photo | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.executorIdAddphotoPost(id, photo, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **photo** | [**Photo**](Photo.md)|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="executorIdDelete"></a>
# **executorIdDelete**
> executorIdDelete(id)



Удаление исполнителя

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.executorIdDelete(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="executorIdGet"></a>
# **executorIdGet**
> ExecFullItem executorIdGet(id)



Получение информации об исполнителе

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.executorIdGet(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

[**ExecFullItem**](ExecFullItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="executorIdOffersGet"></a>
# **executorIdOffersGet**
> FoundedOffersList executorIdOffersGet(id)



Список предложений данного исполнителя

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.executorIdOffersGet(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

[**FoundedOffersList**](FoundedOffersList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="executorIdOrdersGet"></a>
# **executorIdOrdersGet**
> FoundedOrdersList executorIdOrdersGet(id)



Заказы, связанные с данным исполнителем

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.executorIdOrdersGet(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

[**FoundedOrdersList**](FoundedOrdersList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="executorIdPut"></a>
# **executorIdPut**
> executorIdPut(id, executorRenewObject)



Обновление исполнителя

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | 

var executorRenewObject = new MasterBoxFreelanceExchange.ExecFullItem(); // ExecFullItem | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.executorIdPut(id, executorRenewObject, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **executorRenewObject** | [**ExecFullItem**](ExecFullItem.md)|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="executorIdSettingsEduPatch"></a>
# **executorIdSettingsEduPatch**
> ExecFullItem executorIdSettingsEduPatch(id, executorNewSkills)



Изменение информации об образовании исполнителя

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | 

var executorNewSkills = new MasterBoxFreelanceExchange.ExecutorNewSkills(); // ExecutorNewSkills | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.executorIdSettingsEduPatch(id, executorNewSkills, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **executorNewSkills** | [**ExecutorNewSkills**](ExecutorNewSkills.md)|  | 

### Return type

[**ExecFullItem**](ExecFullItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="executorIdSettingsEmailPatch"></a>
# **executorIdSettingsEmailPatch**
> ExecFullItem executorIdSettingsEmailPatch(id, executorNewMail)



Изменение e-mail исполнителя

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | 

var executorNewMail = new MasterBoxFreelanceExchange.ExecutorNewMail(); // ExecutorNewMail | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.executorIdSettingsEmailPatch(id, executorNewMail, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **executorNewMail** | [**ExecutorNewMail**](ExecutorNewMail.md)|  | 

### Return type

[**ExecFullItem**](ExecFullItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="executorIdSettingsNamePatch"></a>
# **executorIdSettingsNamePatch**
> ExecFullItem executorIdSettingsNamePatch(id, executorNewName)



Изменение имени исполнителя

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | 

var executorNewName = new MasterBoxFreelanceExchange.ExecutorNewName(); // ExecutorNewName | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.executorIdSettingsNamePatch(id, executorNewName, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **executorNewName** | [**ExecutorNewName**](ExecutorNewName.md)|  | 

### Return type

[**ExecFullItem**](ExecFullItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="executorIdSettingsPasswdPatch"></a>
# **executorIdSettingsPasswdPatch**
> ExecFullItem executorIdSettingsPasswdPatch(id, executorNewPassword)



Изменение пароля исполнителя

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | 

var executorNewPassword = new MasterBoxFreelanceExchange.ExecutorNewPassword(); // ExecutorNewPassword | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.executorIdSettingsPasswdPatch(id, executorNewPassword, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **executorNewPassword** | [**ExecutorNewPassword**](ExecutorNewPassword.md)|  | 

### Return type

[**ExecFullItem**](ExecFullItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="executorIdSettingsPhonePatch"></a>
# **executorIdSettingsPhonePatch**
> ExecFullItem executorIdSettingsPhonePatch(id, executorNewPhone)



Изменение номера телефона исполнителя

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | 

var executorNewPhone = new MasterBoxFreelanceExchange.ExecutorNewPhone(); // ExecutorNewPhone | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.executorIdSettingsPhonePatch(id, executorNewPhone, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **executorNewPhone** | [**ExecutorNewPhone**](ExecutorNewPhone.md)|  | 

### Return type

[**ExecFullItem**](ExecFullItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="executorIdSettingsSkillsPatch"></a>
# **executorIdSettingsSkillsPatch**
> ExecFullItem executorIdSettingsSkillsPatch(id, executorNewSkills)



Изменение навыков исполнителя

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | 

var executorNewSkills = [new MasterBoxFreelanceExchange.[String]()]; // [String] | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.executorIdSettingsSkillsPatch(id, executorNewSkills, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **executorNewSkills** | **[String]**|  | 

### Return type

[**ExecFullItem**](ExecFullItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="executorIdStatOrderstatGet"></a>
# **executorIdStatOrderstatGet**
> InlineResponse2005 executorIdStatOrderstatGet(id)



### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.executorIdStatOrderstatGet(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="executorIdStatRatingstatGet"></a>
# **executorIdStatRatingstatGet**
> InlineResponse2004 executorIdStatRatingstatGet(id)



### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.executorIdStatRatingstatGet(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="executorPost"></a>
# **executorPost**
> executorPost(executorAdd)



Добавление исполнителя

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var executorAdd = new MasterBoxFreelanceExchange.ExecFullItem(); // ExecFullItem | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.executorPost(executorAdd, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **executorAdd** | [**ExecFullItem**](ExecFullItem.md)|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="messagesIdGet"></a>
# **messagesIdGet**
> ExtendedMessage messagesIdGet(id)

Получение сообщения по id

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | id сообщения


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.messagesIdGet(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| id сообщения | 

### Return type

[**ExtendedMessage**](ExtendedMessage.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="offersIdDelete"></a>
# **offersIdDelete**
> offersIdDelete(id)



Удаление предложения по ID

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.offersIdDelete(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="offersIdGet"></a>
# **offersIdGet**
> OfferFullItem offersIdGet(id)



Получение предложения по ID

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.offersIdGet(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

[**OfferFullItem**](OfferFullItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="offersIdPut"></a>
# **offersIdPut**
> offersIdPut(id, offserChangeSettings)



Обновление предложения

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | 

var offserChangeSettings = new MasterBoxFreelanceExchange.OfferFullItem(); // OfferFullItem | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.offersIdPut(id, offserChangeSettings, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **offserChangeSettings** | [**OfferFullItem**](OfferFullItem.md)|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="offersPost"></a>
# **offersPost**
> InlineResponse2001 offersPost(osserDetails)



Добавление нового предложения

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var osserDetails = new MasterBoxFreelanceExchange.OfferFullItem(); // OfferFullItem | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.offersPost(osserDetails, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **osserDetails** | [**OfferFullItem**](OfferFullItem.md)|  | 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="orderIdRespondsGet"></a>
# **orderIdRespondsGet**
> [ExtendedRespond] orderIdRespondsGet(id)

Получить отклики по id заказа

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | id заказа


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.orderIdRespondsGet(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| id заказа | 

### Return type

[**[ExtendedRespond]**](ExtendedRespond.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="orderIdRespondsPost"></a>
# **orderIdRespondsPost**
> ExtendedRespond orderIdRespondsPost(id, respond)

Добавить отклик в заказ по id

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | id заказа

var respond = new MasterBoxFreelanceExchange.BasicRespond(); // BasicRespond | id заказа


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.orderIdRespondsPost(id, respond, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| id заказа | 
 **respond** | [**BasicRespond**](BasicRespond.md)| id заказа | 

### Return type

[**ExtendedRespond**](ExtendedRespond.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="ordersGet"></a>
# **ordersGet**
> [ExtendedOrder] ordersGet()

Получение списка заказов заказчика

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.ordersGet(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[ExtendedOrder]**](ExtendedOrder.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="ordersIdDelete"></a>
# **ordersIdDelete**
> ExtendedOrder ordersIdDelete(id)

Изменение заказа

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.ordersIdDelete(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

[**ExtendedOrder**](ExtendedOrder.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="ordersIdFeedbacksGet"></a>
# **ordersIdFeedbacksGet**
> [ExtendedFeedback] ordersIdFeedbacksGet(id)

Отзывы заказчика и исполнителя к заказу

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | id заказа


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.ordersIdFeedbacksGet(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| id заказа | 

### Return type

[**[ExtendedFeedback]**](ExtendedFeedback.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="ordersIdGet"></a>
# **ordersIdGet**
> ExtendedOrder ordersIdGet(id)

Получение подробного заказа заказчика

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.ordersIdGet(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

[**ExtendedOrder**](ExtendedOrder.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="ordersIdMessagesDelete"></a>
# **ordersIdMessagesDelete**
> ordersIdMessagesDelete(id)

Удалить сообщения, связанные с заказом

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | id заказа


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.ordersIdMessagesDelete(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| id заказа | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="ordersIdMessagesGet"></a>
# **ordersIdMessagesGet**
> [ExtendedMessage] ordersIdMessagesGet(id)

Получение сообщений заказа

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | id заказа


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.ordersIdMessagesGet(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| id заказа | 

### Return type

[**[ExtendedMessage]**](ExtendedMessage.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="ordersIdMessagesPost"></a>
# **ordersIdMessagesPost**
> ExtendedMessage ordersIdMessagesPost(id)

Добавить сообщение к заказу

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | id заказа


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.ordersIdMessagesPost(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| id заказа | 

### Return type

[**ExtendedMessage**](ExtendedMessage.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="ordersIdPut"></a>
# **ordersIdPut**
> ExtendedOrder ordersIdPut(id, order)

Изменение заказа

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | 

var order = new MasterBoxFreelanceExchange.BasicOrder(); // BasicOrder | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.ordersIdPut(id, order, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **order** | [**BasicOrder**](BasicOrder.md)|  | 

### Return type

[**ExtendedOrder**](ExtendedOrder.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="ordersPost"></a>
# **ordersPost**
> ExtendedOrder ordersPost(order)

Добавление заказа

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var order = new MasterBoxFreelanceExchange.BasicOrder(); // BasicOrder | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.ordersPost(order, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order** | [**BasicOrder**](BasicOrder.md)|  | 

### Return type

[**ExtendedOrder**](ExtendedOrder.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="respondsIdDelete"></a>
# **respondsIdDelete**
> respondsIdDelete(id)

Получить отклик по id

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | id отклика


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.respondsIdDelete(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| id отклика | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="respondsIdGet"></a>
# **respondsIdGet**
> ExtendedRespond respondsIdGet(id)

Получить отклик по id

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | id отклика


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.respondsIdGet(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| id отклика | 

### Return type

[**ExtendedRespond**](ExtendedRespond.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="respondsIdPut"></a>
# **respondsIdPut**
> ExtendedRespond respondsIdPut(id, respond)

Изменить отклик по id

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | id отклика

var respond = new MasterBoxFreelanceExchange.BasicRespond(); // BasicRespond | Изменённый объект отклика


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.respondsIdPut(id, respond, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| id отклика | 
 **respond** | [**BasicRespond**](BasicRespond.md)| Изменённый объект отклика | 

### Return type

[**ExtendedRespond**](ExtendedRespond.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="searchOffersGet"></a>
# **searchOffersGet**
> FoundedOffersList searchOffersGet(query, opts)



### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var query = "query_example"; // String | Строка поискового запроса

var opts = { 
  'category': 56, // Number | Категория
  'minCost': "minCost_example", // String | Минимальная плата
  'maxCost': "maxCost_example", // String | Максимальная плата
  'dueTime': "dueTime_example" // String | Максимальный срок исполнения (часов)
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.searchOffersGet(query, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | **String**| Строка поискового запроса | 
 **category** | **Number**| Категория | [optional] 
 **minCost** | **String**| Минимальная плата | [optional] 
 **maxCost** | **String**| Максимальная плата | [optional] 
 **dueTime** | **String**| Максимальный срок исполнения (часов) | [optional] 

### Return type

[**FoundedOffersList**](FoundedOffersList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="searchOrdersGet"></a>
# **searchOrdersGet**
> FoundedOrdersList searchOrdersGet(query, opts)



### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var query = "query_example"; // String | Строка поискового запроса

var opts = { 
  'category': 56, // Number | Категория
  'minCost': "minCost_example", // String | Минимальная плата
  'maxCost': "maxCost_example", // String | Максимальная плата
  'dueTime': "dueTime_example" // String | Максимальный срок исполнения (часов)
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.searchOrdersGet(query, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | **String**| Строка поискового запроса | 
 **category** | **Number**| Категория | [optional] 
 **minCost** | **String**| Минимальная плата | [optional] 
 **maxCost** | **String**| Максимальная плата | [optional] 
 **dueTime** | **String**| Максимальный срок исполнения (часов) | [optional] 

### Return type

[**FoundedOrdersList**](FoundedOrdersList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="usersCustomersGet"></a>
# **usersCustomersGet**
> [ExtendedOrder] usersCustomersGet()

Получение всех пользователей-заказчиков

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.usersCustomersGet(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[ExtendedOrder]**](ExtendedOrder.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="usersCustomersPost"></a>
# **usersCustomersPost**
> ExtendedCustomer usersCustomersPost(order)

Добавление нового пользователя как заказчика

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var order = new MasterBoxFreelanceExchange.Order(); // Order | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.usersCustomersPost(order, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order** | [**Order**](Order.md)|  | 

### Return type

[**ExtendedCustomer**](ExtendedCustomer.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="usersIdDelete"></a>
# **usersIdDelete**
> usersIdDelete(id)

Удаление пользователя

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.usersIdDelete(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="usersIdFeedbacksCustomerPost"></a>
# **usersIdFeedbacksCustomerPost**
> ExtendedFeedback usersIdFeedbacksCustomerPost(id, feedback)

Добавить отзыв от лица заказчика

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | id заказа

var feedback = new MasterBoxFreelanceExchange.BasicFeedback(); // BasicFeedback | Новый объект отзыва


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.usersIdFeedbacksCustomerPost(id, feedback, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| id заказа | 
 **feedback** | [**BasicFeedback**](BasicFeedback.md)| Новый объект отзыва | 

### Return type

[**ExtendedFeedback**](ExtendedFeedback.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="usersIdFeedbacksWorkerPost"></a>
# **usersIdFeedbacksWorkerPost**
> ExtendedFeedback usersIdFeedbacksWorkerPost(id, feedback)

Добавить отзыв от лица исполнителя

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | id заказа

var feedback = new MasterBoxFreelanceExchange.BasicFeedback(); // BasicFeedback | Новый объект отзыва


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.usersIdFeedbacksWorkerPost(id, feedback, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| id заказа | 
 **feedback** | [**BasicFeedback**](BasicFeedback.md)| Новый объект отзыва | 

### Return type

[**ExtendedFeedback**](ExtendedFeedback.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="usersIdGet"></a>
# **usersIdGet**
> ExtendedCustomer usersIdGet(id)

Получение пользователя

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.usersIdGet(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

[**ExtendedCustomer**](ExtendedCustomer.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="usersIdMessagesGet"></a>
# **usersIdMessagesGet**
> usersIdMessagesGet(id)

Получение сообщений пользователя

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | id заказа


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.usersIdMessagesGet(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| id заказа | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="usersIdSettingSetAvatarPatch"></a>
# **usersIdSettingSetAvatarPatch**
> usersIdSettingSetAvatarPatch(id, avatarImg)



Изменение пароля

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | 

var avatarImg = new MasterBoxFreelanceExchange.AvatarImg(); // AvatarImg | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.usersIdSettingSetAvatarPatch(id, avatarImg, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **avatarImg** | [**AvatarImg**](AvatarImg.md)|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="usersIdSettingSetEmailPatch"></a>
# **usersIdSettingSetEmailPatch**
> usersIdSettingSetEmailPatch(id, password)



Изменение email

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | 

var password = new MasterBoxFreelanceExchange.Password1(); // Password1 | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.usersIdSettingSetEmailPatch(id, password, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **password** | [**Password1**](Password1.md)|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="usersIdSettingSetNameSurnamePatch"></a>
# **usersIdSettingSetNameSurnamePatch**
> usersIdSettingSetNameSurnamePatch(id, nameSurname)



Изменение имени и фамилии

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | 

var nameSurname = new MasterBoxFreelanceExchange.NameSurname(); // NameSurname | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.usersIdSettingSetNameSurnamePatch(id, nameSurname, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **nameSurname** | [**NameSurname**](NameSurname.md)|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="usersIdSettingSetPasswordPatch"></a>
# **usersIdSettingSetPasswordPatch**
> usersIdSettingSetPasswordPatch(id, password)



Изменение пароля

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | 

var password = new MasterBoxFreelanceExchange.Password(); // Password | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.usersIdSettingSetPasswordPatch(id, password, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **password** | [**Password**](Password.md)|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="usersIdSettingSetPhonePatch"></a>
# **usersIdSettingSetPhonePatch**
> usersIdSettingSetPhonePatch(id, phone)



Изменение номера телефона

### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange.DefaultApi();

var id = 56; // Number | 

var phone = new MasterBoxFreelanceExchange.Phone(); // Phone | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.usersIdSettingSetPhonePatch(id, phone, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **phone** | [**Phone**](Phone.md)|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

