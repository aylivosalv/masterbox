# MasterBoxFreelanceExchange.Reg

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** | Почта для регистрации учетной записи | 
**password** | **String** | Пароль для новой учетной записи | 
**accountType** | **String** | Тип аккаунта пользователя | [optional] 
**name** | **String** | Фио пользователя | 
**city** | **String** | Город | [optional] 
**areas** | **[String]** |  | [optional] 


