# MasterBoxFreelanceExchange.InlineResponse2003

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Код категории | [optional] 
**parent** | **Number** | Код родительской категории, если не существует - 0 | [optional] 
**name** | **String** | Название категории | [optional] 


