# MasterBoxFreelanceExchange.ExecFullItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | ФИО исполнителя | 
**skills** | **String** | Профессиональные навыки | 
**edu** | **String** | Профессиональные навыки | [optional] 
**email** | **String** | Электронная почта | 
**phone** | **String** | Номер телефона | 


