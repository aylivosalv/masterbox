# MasterBoxFreelanceExchange._Api

All URIs are relative to *https://virtserver.swaggerhub.com/aylivosalv/MasterBox/0.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**loginPost**](_Api.md#loginPost) | **POST** /login | 
[**registerPost**](_Api.md#registerPost) | **POST** /register | 


<a name="loginPost"></a>
# **loginPost**
> InlineResponse200 loginPost(login)



### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange._Api();

var login = new MasterBoxFreelanceExchange.Login(); // Login | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.loginPost(login, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **login** | [**Login**](Login.md)|  | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="registerPost"></a>
# **registerPost**
> InlineResponse200 registerPost(reg)



### Example
```javascript
var MasterBoxFreelanceExchange = require('master_box___freelance_exchange');

var apiInstance = new MasterBoxFreelanceExchange._Api();

var reg = new MasterBoxFreelanceExchange.Reg(); // Reg | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.registerPost(reg, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reg** | [**Reg**](Reg.md)|  | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

