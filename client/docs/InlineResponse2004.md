# MasterBoxFreelanceExchange.InlineResponse2004

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**feedbackCount** | **Number** | Количество отзывов | [optional] 
**positiveFeedbackCount** | **Number** | Количество положительных отзывов | [optional] 
**negativeFeedbackCount** | **Number** | Количество отрицательных отзывов | [optional] 
**meanRating** | **Number** | Средний рейтинг | [optional] 


