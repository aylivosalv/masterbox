# MasterBoxFreelanceExchange.InlineResponse2005

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orderTotalCount** | **Number** | Общее количество заказов | [optional] 
**orderTerminatedCount** | **Number** | Количество завершенных заказов | [optional] 
**orderProcessingCount** | **Number** | Количество исполняемых заказов | [optional] 
**orderCancelledCount** | **Number** | Количество отмененных заказов | [optional] 


