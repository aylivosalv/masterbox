# MasterBoxFreelanceExchange.BasicFeedback

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**text** | **String** | Описание отзыва | [optional] 
**raiting** | **Number** | Оценка заказа (1 - ужасно, 5 - отлично) | 


