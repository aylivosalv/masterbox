# MasterBoxFreelanceExchange.BasicCustomer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Имя заказчика | 
**surname** | **String** | Фамилия заказчика | 
**phone** | **String** | Телефон заказчика | 
**email** | **String** | Email заказчика | 


