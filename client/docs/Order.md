# MasterBoxFreelanceExchange.Order

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | **Boolean** | В случае успешного ответа | [optional] 
**data** | [**BasicCustomer**](BasicCustomer.md) |  | [optional] 


