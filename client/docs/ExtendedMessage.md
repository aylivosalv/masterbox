# MasterBoxFreelanceExchange.ExtendedMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | id сообщения | 
**senderId** | **Number** | id отправителя сообщения | 
**senderName** | **String** | Имя отправителя сообщения | 
**orderId** | **Number** | id заказа, за которым закреплено сообщение | 
**sendingDate** | **String** | Время отправки сообщения. Формат {dd-MM-yyyy ss:mm:hh} | 


