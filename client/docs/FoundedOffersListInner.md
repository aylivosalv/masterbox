# MasterBoxFreelanceExchange.FoundedOffersListInner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | **Number** | Числовое значение категории, к которой принадлежит данное предложение | 
**name** | **String** | Название нового предложения | 
**offerDescription** | **String** | Описание предложения | [optional] 
**timeExec** | **Number** | Время исполнения | [optional] 
**measurements** | **String** | Единицы измерения времени | [optional] 
**price** | **Number** | Стоимость выполнения заказа | 
**currency** | **String** | Наименование валюты | 


