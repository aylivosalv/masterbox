# MasterBoxFreelanceExchange.ExtendedFeedback

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | **Number** | id пользователя | 
**orderId** | **Number** | id заказа | 
**id** | **Number** | id отзыва | 


