# MasterBoxFreelanceExchange.FoundedOrdersListInner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** | Заголовок заказа | 
**category** | **Number** | Численное перечисление для категорий | 
**photos** | **Blob** | Загрузка фотографий для заказа | [optional] 
**location** | **String** | Адрес выполнения заказа | [optional] 
**dueTime** | **Number** | Максимальное количество времени на исполнение заказа | 
**dueTimeType** | **Number** | Числовое перечисление для типа времени исполнения | 
**costFrom** | **Number** | Минимальная сумма, которую готов отдать заказчик | [optional] 
**costTo** | **Number** | Максимальная сумма, которую готов отдать заказчик | 


