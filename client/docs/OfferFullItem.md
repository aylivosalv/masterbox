# MasterBoxFreelanceExchange.OfferFullItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | **Number** | Численное значение идентификатора категории | 
**name** | **String** | Название нового предложения | 
**offerDescription** | **String** | Описание предложения | [optional] 
**timeExec** | **Number** | Время исполнения | [optional] 
**measurements** | **Number** | Числовое значение единиц измерения времени | [optional] 
**price** | **Number** | Стоимость выполнения заказа | 
**currency** | **String** | Числовое значение наименования валюты | 
**paymentMethod** | **String** | Числовое значение метода оплаты | [optional] 
**tariff** | **Boolean** | Поддержка тарификации | [optional] 
**departureEnabled** | **Boolean** | Возможность выезда | [optional] 
**remotely** | **Boolean** | Возможность удаленного исполнения | [optional] 


