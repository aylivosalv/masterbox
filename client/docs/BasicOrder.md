# MasterBoxFreelanceExchange.BasicOrder

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** | Заголовок заказа | 
**category** | **Number** | Численное перечисление для категорий | 
**description** | **String** | Детальное описание заказа | 
**photos** | **Blob** | Загрузка фотографий для заказа | [optional] 
**location** | **String** | Адрес выполнения заказа | [optional] 
**dueTime** | **Number** | Максимальное количество времени на исполнение заказа | 
**dueTimeType** | **Number** | Максимальное количество времени на исполнение заказа | 
**costFrom** | **Number** | Минимальная сумма, которую готов отдать заказчик | [optional] 
**costTo** | **Number** | Максимальная сумма, которую готов отдать заказчик | 


