/*
 * MasterBox - Freelance exchange
 * Freelance exchange service
 *
 * OpenAPI spec version: 0.1
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.13
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.MasterBoxFreelanceExchange);
  }
}(this, function(expect, MasterBoxFreelanceExchange) {
  'use strict';

  var instance;

  describe('(package)', function() {
    describe('ExecutorNewSkills', function() {
      beforeEach(function() {
        instance = new MasterBoxFreelanceExchange.ExecutorNewSkills();
      });

      it('should create an instance of ExecutorNewSkills', function() {
        // TODO: update the code to test ExecutorNewSkills
        expect(instance).to.be.a(MasterBoxFreelanceExchange.ExecutorNewSkills);
      });

      it('should have the property edu (base name: "edu")', function() {
        // TODO: update the code to test the property edu
        expect(instance).to.have.property('edu');
        // expect(instance.edu).to.be(expectedValueLiteral);
      });

    });
  });

}));
