/*
 * MasterBox - Freelance exchange
 * Freelance exchange service
 *
 * OpenAPI spec version: 0.1
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.13
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.MasterBoxFreelanceExchange);
  }
}(this, function(expect, MasterBoxFreelanceExchange) {
  'use strict';

  var instance;

  describe('(package)', function() {
    describe('FoundedOrdersListInner', function() {
      beforeEach(function() {
        instance = new MasterBoxFreelanceExchange.FoundedOrdersListInner();
      });

      it('should create an instance of FoundedOrdersListInner', function() {
        // TODO: update the code to test FoundedOrdersListInner
        expect(instance).to.be.a(MasterBoxFreelanceExchange.FoundedOrdersListInner);
      });

      it('should have the property title (base name: "title")', function() {
        // TODO: update the code to test the property title
        expect(instance).to.have.property('title');
        // expect(instance.title).to.be(expectedValueLiteral);
      });

      it('should have the property category (base name: "category")', function() {
        // TODO: update the code to test the property category
        expect(instance).to.have.property('category');
        // expect(instance.category).to.be(expectedValueLiteral);
      });

      it('should have the property photos (base name: "photos")', function() {
        // TODO: update the code to test the property photos
        expect(instance).to.have.property('photos');
        // expect(instance.photos).to.be(expectedValueLiteral);
      });

      it('should have the property location (base name: "location")', function() {
        // TODO: update the code to test the property location
        expect(instance).to.have.property('location');
        // expect(instance.location).to.be(expectedValueLiteral);
      });

      it('should have the property dueTime (base name: "due_time")', function() {
        // TODO: update the code to test the property dueTime
        expect(instance).to.have.property('dueTime');
        // expect(instance.dueTime).to.be(expectedValueLiteral);
      });

      it('should have the property dueTimeType (base name: "due_time_type")', function() {
        // TODO: update the code to test the property dueTimeType
        expect(instance).to.have.property('dueTimeType');
        // expect(instance.dueTimeType).to.be(expectedValueLiteral);
      });

      it('should have the property costFrom (base name: "cost_from")', function() {
        // TODO: update the code to test the property costFrom
        expect(instance).to.have.property('costFrom');
        // expect(instance.costFrom).to.be(expectedValueLiteral);
      });

      it('should have the property costTo (base name: "cost_to")', function() {
        // TODO: update the code to test the property costTo
        expect(instance).to.have.property('costTo');
        // expect(instance.costTo).to.be(expectedValueLiteral);
      });

    });
  });

}));
