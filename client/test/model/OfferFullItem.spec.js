/*
 * MasterBox - Freelance exchange
 * Freelance exchange service
 *
 * OpenAPI spec version: 0.1
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.13
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.MasterBoxFreelanceExchange);
  }
}(this, function(expect, MasterBoxFreelanceExchange) {
  'use strict';

  var instance;

  describe('(package)', function() {
    describe('OfferFullItem', function() {
      beforeEach(function() {
        instance = new MasterBoxFreelanceExchange.OfferFullItem();
      });

      it('should create an instance of OfferFullItem', function() {
        // TODO: update the code to test OfferFullItem
        expect(instance).to.be.a(MasterBoxFreelanceExchange.OfferFullItem);
      });

      it('should have the property category (base name: "category")', function() {
        // TODO: update the code to test the property category
        expect(instance).to.have.property('category');
        // expect(instance.category).to.be(expectedValueLiteral);
      });

      it('should have the property name (base name: "name")', function() {
        // TODO: update the code to test the property name
        expect(instance).to.have.property('name');
        // expect(instance.name).to.be(expectedValueLiteral);
      });

      it('should have the property offerDescription (base name: "offerDescription")', function() {
        // TODO: update the code to test the property offerDescription
        expect(instance).to.have.property('offerDescription');
        // expect(instance.offerDescription).to.be(expectedValueLiteral);
      });

      it('should have the property timeExec (base name: "timeExec")', function() {
        // TODO: update the code to test the property timeExec
        expect(instance).to.have.property('timeExec');
        // expect(instance.timeExec).to.be(expectedValueLiteral);
      });

      it('should have the property measurements (base name: "measurements")', function() {
        // TODO: update the code to test the property measurements
        expect(instance).to.have.property('measurements');
        // expect(instance.measurements).to.be(expectedValueLiteral);
      });

      it('should have the property price (base name: "price")', function() {
        // TODO: update the code to test the property price
        expect(instance).to.have.property('price');
        // expect(instance.price).to.be(expectedValueLiteral);
      });

      it('should have the property currency (base name: "currency")', function() {
        // TODO: update the code to test the property currency
        expect(instance).to.have.property('currency');
        // expect(instance.currency).to.be(expectedValueLiteral);
      });

      it('should have the property paymentMethod (base name: "paymentMethod")', function() {
        // TODO: update the code to test the property paymentMethod
        expect(instance).to.have.property('paymentMethod');
        // expect(instance.paymentMethod).to.be(expectedValueLiteral);
      });

      it('should have the property tariff (base name: "tariff")', function() {
        // TODO: update the code to test the property tariff
        expect(instance).to.have.property('tariff');
        // expect(instance.tariff).to.be(expectedValueLiteral);
      });

      it('should have the property departureEnabled (base name: "departureEnabled")', function() {
        // TODO: update the code to test the property departureEnabled
        expect(instance).to.have.property('departureEnabled');
        // expect(instance.departureEnabled).to.be(expectedValueLiteral);
      });

      it('should have the property remotely (base name: "remotely")', function() {
        // TODO: update the code to test the property remotely
        expect(instance).to.have.property('remotely');
        // expect(instance.remotely).to.be(expectedValueLiteral);
      });

    });
  });

}));
