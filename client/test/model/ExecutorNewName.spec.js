/*
 * MasterBox - Freelance exchange
 * Freelance exchange service
 *
 * OpenAPI spec version: 0.1
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.13
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.MasterBoxFreelanceExchange);
  }
}(this, function(expect, MasterBoxFreelanceExchange) {
  'use strict';

  var instance;

  describe('(package)', function() {
    describe('ExecutorNewName', function() {
      beforeEach(function() {
        instance = new MasterBoxFreelanceExchange.ExecutorNewName();
      });

      it('should create an instance of ExecutorNewName', function() {
        // TODO: update the code to test ExecutorNewName
        expect(instance).to.be.a(MasterBoxFreelanceExchange.ExecutorNewName);
      });

      it('should have the property newName (base name: "newName")', function() {
        // TODO: update the code to test the property newName
        expect(instance).to.have.property('newName');
        // expect(instance.newName).to.be(expectedValueLiteral);
      });

    });
  });

}));
