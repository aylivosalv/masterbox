/*
 * MasterBox - Freelance exchange
 * Freelance exchange service
 *
 * OpenAPI spec version: 0.1
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.13
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.MasterBoxFreelanceExchange) {
      root.MasterBoxFreelanceExchange = {};
    }
    root.MasterBoxFreelanceExchange.Login = factory(root.MasterBoxFreelanceExchange.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';

  /**
   * The Login model module.
   * @module model/Login
   * @version 0.1
   */

  /**
   * Constructs a new <code>Login</code>.
   * @alias module:model/Login
   * @class
   * @param email {String} Электронная почта
   * @param password {String} Пароль
   */
  var exports = function(email, password) {
    this.email = email;
    this.password = password;
  };

  /**
   * Constructs a <code>Login</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/Login} obj Optional instance to populate.
   * @return {module:model/Login} The populated <code>Login</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('email'))
        obj.email = ApiClient.convertToType(data['email'], 'String');
      if (data.hasOwnProperty('password'))
        obj.password = ApiClient.convertToType(data['password'], 'String');
    }
    return obj;
  }

  /**
   * Электронная почта
   * @member {String} email
   */
  exports.prototype.email = undefined;

  /**
   * Пароль
   * @member {String} password
   */
  exports.prototype.password = undefined;

  return exports;

}));
