/*
 * MasterBox - Freelance exchange
 * Freelance exchange service
 *
 * OpenAPI spec version: 0.1
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.13
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.MasterBoxFreelanceExchange) {
      root.MasterBoxFreelanceExchange = {};
    }
    root.MasterBoxFreelanceExchange.BasicFeedback = factory(root.MasterBoxFreelanceExchange.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';

  /**
   * The BasicFeedback model module.
   * @module model/BasicFeedback
   * @version 0.1
   */

  /**
   * Constructs a new <code>BasicFeedback</code>.
   * @alias module:model/BasicFeedback
   * @class
   * @param raiting {Number} Оценка заказа (1 - ужасно, 5 - отлично)
   */
  var exports = function(raiting) {
    this.raiting = raiting;
  };

  /**
   * Constructs a <code>BasicFeedback</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/BasicFeedback} obj Optional instance to populate.
   * @return {module:model/BasicFeedback} The populated <code>BasicFeedback</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('text'))
        obj.text = ApiClient.convertToType(data['text'], 'String');
      if (data.hasOwnProperty('raiting'))
        obj.raiting = ApiClient.convertToType(data['raiting'], 'Number');
    }
    return obj;
  }

  /**
   * Описание отзыва
   * @member {String} text
   */
  exports.prototype.text = undefined;

  /**
   * Оценка заказа (1 - ужасно, 5 - отлично)
   * @member {Number} raiting
   */
  exports.prototype.raiting = undefined;

  return exports;

}));
