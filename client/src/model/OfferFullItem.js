/*
 * MasterBox - Freelance exchange
 * Freelance exchange service
 *
 * OpenAPI spec version: 0.1
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.13
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.MasterBoxFreelanceExchange) {
      root.MasterBoxFreelanceExchange = {};
    }
    root.MasterBoxFreelanceExchange.OfferFullItem = factory(root.MasterBoxFreelanceExchange.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';

  /**
   * The OfferFullItem model module.
   * @module model/OfferFullItem
   * @version 0.1
   */

  /**
   * Constructs a new <code>OfferFullItem</code>.
   * @alias module:model/OfferFullItem
   * @class
   * @param category {Number} Численное значение идентификатора категории
   * @param name {String} Название нового предложения
   * @param price {Number} Стоимость выполнения заказа
   * @param currency {String} Числовое значение наименования валюты
   */
  var exports = function(category, name, price, currency) {
    this.category = category;
    this.name = name;
    this.price = price;
    this.currency = currency;
  };

  /**
   * Constructs a <code>OfferFullItem</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/OfferFullItem} obj Optional instance to populate.
   * @return {module:model/OfferFullItem} The populated <code>OfferFullItem</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('category'))
        obj.category = ApiClient.convertToType(data['category'], 'Number');
      if (data.hasOwnProperty('name'))
        obj.name = ApiClient.convertToType(data['name'], 'String');
      if (data.hasOwnProperty('offerDescription'))
        obj.offerDescription = ApiClient.convertToType(data['offerDescription'], 'String');
      if (data.hasOwnProperty('timeExec'))
        obj.timeExec = ApiClient.convertToType(data['timeExec'], 'Number');
      if (data.hasOwnProperty('measurements'))
        obj.measurements = ApiClient.convertToType(data['measurements'], 'Number');
      if (data.hasOwnProperty('price'))
        obj.price = ApiClient.convertToType(data['price'], 'Number');
      if (data.hasOwnProperty('currency'))
        obj.currency = ApiClient.convertToType(data['currency'], 'String');
      if (data.hasOwnProperty('paymentMethod'))
        obj.paymentMethod = ApiClient.convertToType(data['paymentMethod'], 'String');
      if (data.hasOwnProperty('tariff'))
        obj.tariff = ApiClient.convertToType(data['tariff'], 'Boolean');
      if (data.hasOwnProperty('departureEnabled'))
        obj.departureEnabled = ApiClient.convertToType(data['departureEnabled'], 'Boolean');
      if (data.hasOwnProperty('remotely'))
        obj.remotely = ApiClient.convertToType(data['remotely'], 'Boolean');
    }
    return obj;
  }

  /**
   * Численное значение идентификатора категории
   * @member {Number} category
   */
  exports.prototype.category = undefined;

  /**
   * Название нового предложения
   * @member {String} name
   */
  exports.prototype.name = undefined;

  /**
   * Описание предложения
   * @member {String} offerDescription
   */
  exports.prototype.offerDescription = undefined;

  /**
   * Время исполнения
   * @member {Number} timeExec
   */
  exports.prototype.timeExec = undefined;

  /**
   * Числовое значение единиц измерения времени
   * @member {Number} measurements
   */
  exports.prototype.measurements = undefined;

  /**
   * Стоимость выполнения заказа
   * @member {Number} price
   */
  exports.prototype.price = undefined;

  /**
   * Числовое значение наименования валюты
   * @member {String} currency
   */
  exports.prototype.currency = undefined;

  /**
   * Числовое значение метода оплаты
   * @member {String} paymentMethod
   */
  exports.prototype.paymentMethod = undefined;

  /**
   * Поддержка тарификации
   * @member {Boolean} tariff
   */
  exports.prototype.tariff = undefined;

  /**
   * Возможность выезда
   * @member {Boolean} departureEnabled
   */
  exports.prototype.departureEnabled = undefined;

  /**
   * Возможность удаленного исполнения
   * @member {Boolean} remotely
   */
  exports.prototype.remotely = undefined;

  return exports;

}));
