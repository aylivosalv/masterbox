/*
 * MasterBox - Freelance exchange
 * Freelance exchange service
 *
 * OpenAPI spec version: 0.1
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.13
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.MasterBoxFreelanceExchange) {
      root.MasterBoxFreelanceExchange = {};
    }
    root.MasterBoxFreelanceExchange.BasicRespond = factory(root.MasterBoxFreelanceExchange.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';

  /**
   * The BasicRespond model module.
   * @module model/BasicRespond
   * @version 0.1
   */

  /**
   * Constructs a new <code>BasicRespond</code>.
   * @alias module:model/BasicRespond
   * @class
   * @param text {String} Текст отклика
   */
  var exports = function(text) {
    this.text = text;
  };

  /**
   * Constructs a <code>BasicRespond</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/BasicRespond} obj Optional instance to populate.
   * @return {module:model/BasicRespond} The populated <code>BasicRespond</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('text'))
        obj.text = ApiClient.convertToType(data['text'], 'String');
    }
    return obj;
  }

  /**
   * Текст отклика
   * @member {String} text
   */
  exports.prototype.text = undefined;

  return exports;

}));
