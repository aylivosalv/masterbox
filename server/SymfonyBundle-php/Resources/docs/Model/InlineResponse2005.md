# InlineResponse2005

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orderTotalCount** | **int** | Общее количество заказов | [optional] 
**orderTerminatedCount** | **int** | Количество завершенных заказов | [optional] 
**orderProcessingCount** | **int** | Количество исполняемых заказов | [optional] 
**orderCancelledCount** | **int** | Количество отмененных заказов | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


