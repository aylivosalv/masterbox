# Reg

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **string** | Почта для регистрации учетной записи | 
**password** | **string** | Пароль для новой учетной записи | 
**accountType** | **string** | Тип аккаунта пользователя | [optional] 
**name** | **string** | Фио пользователя | 
**city** | **string** | Город | [optional] 
**areas** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


