# OfferFullItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | **int** | Численное значение идентификатора категории | 
**name** | **string** | Название нового предложения | 
**offerDescription** | **string** | Описание предложения | [optional] 
**timeExec** | **int** | Время исполнения | [optional] 
**measurements** | **int** | Числовое значение единиц измерения времени | [optional] 
**price** | **float** | Стоимость выполнения заказа | 
**currency** | **string** | Числовое значение наименования валюты | 
**paymentMethod** | **string** | Числовое значение метода оплаты | [optional] 
**tariff** | **bool** | Поддержка тарификации | [optional] 
**departureEnabled** | **bool** | Возможность выезда | [optional] 
**remotely** | **bool** | Возможность удаленного исполнения | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


