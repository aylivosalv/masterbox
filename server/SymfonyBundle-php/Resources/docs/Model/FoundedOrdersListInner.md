# FoundedOrdersListInner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **string** | Заголовок заказа | 
**category** | **int** | Численное перечисление для категорий | 
**photos** | **string** | Загрузка фотографий для заказа | [optional] 
**location** | **string** | Адрес выполнения заказа | [optional] 
**dueTime** | **int** | Максимальное количество времени на исполнение заказа | 
**dueTimeType** | **int** | Числовое перечисление для типа времени исполнения | 
**costFrom** | **float** | Минимальная сумма, которую готов отдать заказчик | [optional] 
**costTo** | **float** | Максимальная сумма, которую готов отдать заказчик | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


