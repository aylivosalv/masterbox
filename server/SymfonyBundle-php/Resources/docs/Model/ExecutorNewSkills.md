# ExecutorNewSkills

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**edu** | **string** | Информация об образовании исполнителя | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


