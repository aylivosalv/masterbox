# ExecFullItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | ФИО исполнителя | 
**skills** | **string** | Профессиональные навыки | 
**edu** | **string** | Профессиональные навыки | [optional] 
**email** | **string** | Электронная почта | 
**phone** | **string** | Номер телефона | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


