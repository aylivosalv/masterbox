# BasicCustomer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Имя заказчика | 
**surname** | **string** | Фамилия заказчика | 
**phone** | **string** | Телефон заказчика | 
**email** | **string** | Email заказчика | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


