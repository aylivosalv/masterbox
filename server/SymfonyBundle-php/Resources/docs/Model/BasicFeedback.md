# BasicFeedback

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**text** | **string** | Описание отзыва | [optional] 
**raiting** | **int** | Оценка заказа (1 - ужасно, 5 - отлично) | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


