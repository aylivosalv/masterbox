# ExtendedMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id сообщения | 
**senderId** | **int** | id отправителя сообщения | 
**senderName** | **string** | Имя отправителя сообщения | 
**orderId** | **int** | id заказа, за которым закреплено сообщение | 
**sendingDate** | **string** | Время отправки сообщения. Формат {dd-MM-yyyy ss:mm:hh} | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


