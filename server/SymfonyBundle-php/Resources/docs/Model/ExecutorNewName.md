# ExecutorNewName

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**newName** | **string** | Новое отображаемое имя | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


