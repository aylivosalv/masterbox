# ExecutorNewPhone

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mail** | **string** | Номер телефона исполнителя | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


