# InlineResponse2004

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**feedbackCount** | **int** | Количество отзывов | [optional] 
**positiveFeedbackCount** | **int** | Количество положительных отзывов | [optional] 
**negativeFeedbackCount** | **int** | Количество отрицательных отзывов | [optional] 
**meanRating** | **float** | Средний рейтинг | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


