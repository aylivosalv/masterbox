# ExtendedFeedback

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | **int** | id пользователя | 
**orderId** | **int** | id заказа | 
**id** | **int** | id отзыва | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


