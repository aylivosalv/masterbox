# AvatarImg

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**avatar** | **string** | Новый аватар | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


