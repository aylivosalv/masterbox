# FoundedOffersListInner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | **int** | Числовое значение категории, к которой принадлежит данное предложение | 
**name** | **string** | Название нового предложения | 
**offerDescription** | **string** | Описание предложения | [optional] 
**timeExec** | **int** | Время исполнения | [optional] 
**measurements** | **string** | Единицы измерения времени | [optional] 
**price** | **float** | Стоимость выполнения заказа | 
**currency** | **string** | Наименование валюты | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


