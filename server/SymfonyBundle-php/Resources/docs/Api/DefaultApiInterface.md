# Swagger\Server\Api\DefaultApiInterface

All URIs are relative to *https://virtserver.swaggerhub.com/aylivosalv/MasterBox/0.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**categoriesGet**](DefaultApiInterface.md#categoriesGet) | **GET** /categories | 
[**citiesGet**](DefaultApiInterface.md#citiesGet) | **GET** /cities | 
[**executorIdAddphotoPost**](DefaultApiInterface.md#executorIdAddphotoPost) | **POST** /executor/{id}/addphoto | 
[**executorIdDelete**](DefaultApiInterface.md#executorIdDelete) | **DELETE** /executor/{id} | 
[**executorIdGet**](DefaultApiInterface.md#executorIdGet) | **GET** /executor/{id} | 
[**executorIdOffersGet**](DefaultApiInterface.md#executorIdOffersGet) | **GET** /executor/{id}/offers | 
[**executorIdOrdersGet**](DefaultApiInterface.md#executorIdOrdersGet) | **GET** /executor/{id}/orders | 
[**executorIdPut**](DefaultApiInterface.md#executorIdPut) | **PUT** /executor/{id} | 
[**executorIdSettingsEduPatch**](DefaultApiInterface.md#executorIdSettingsEduPatch) | **PATCH** /executor/{id}/settings/edu | 
[**executorIdSettingsEmailPatch**](DefaultApiInterface.md#executorIdSettingsEmailPatch) | **PATCH** /executor/{id}/settings/email | 
[**executorIdSettingsNamePatch**](DefaultApiInterface.md#executorIdSettingsNamePatch) | **PATCH** /executor/{id}/settings/name | 
[**executorIdSettingsPasswdPatch**](DefaultApiInterface.md#executorIdSettingsPasswdPatch) | **PATCH** /executor/{id}/settings/passwd | 
[**executorIdSettingsPhonePatch**](DefaultApiInterface.md#executorIdSettingsPhonePatch) | **PATCH** /executor/{id}/settings/phone | 
[**executorIdSettingsSkillsPatch**](DefaultApiInterface.md#executorIdSettingsSkillsPatch) | **PATCH** /executor/{id}/settings/skills | 
[**executorIdStatOrderstatGet**](DefaultApiInterface.md#executorIdStatOrderstatGet) | **GET** /executor/{id}/stat/orderstat | 
[**executorIdStatRatingstatGet**](DefaultApiInterface.md#executorIdStatRatingstatGet) | **GET** /executor/{id}/stat/ratingstat | 
[**executorPost**](DefaultApiInterface.md#executorPost) | **POST** /executor | 
[**messagesIdGet**](DefaultApiInterface.md#messagesIdGet) | **GET** /messages/{id} | Получение сообщения по id
[**offersIdDelete**](DefaultApiInterface.md#offersIdDelete) | **DELETE** /offers/{id} | 
[**offersIdGet**](DefaultApiInterface.md#offersIdGet) | **GET** /offers/{id} | 
[**offersIdPut**](DefaultApiInterface.md#offersIdPut) | **PUT** /offers/{id} | 
[**offersPost**](DefaultApiInterface.md#offersPost) | **POST** /offers | 
[**orderIdRespondsGet**](DefaultApiInterface.md#orderIdRespondsGet) | **GET** /order/{id}/responds/ | Получить отклики по id заказа
[**orderIdRespondsPost**](DefaultApiInterface.md#orderIdRespondsPost) | **POST** /order/{id}/responds/ | Добавить отклик в заказ по id
[**ordersGet**](DefaultApiInterface.md#ordersGet) | **GET** /orders/ | Получение списка заказов заказчика
[**ordersIdDelete**](DefaultApiInterface.md#ordersIdDelete) | **DELETE** /orders/{id} | Изменение заказа
[**ordersIdFeedbacksGet**](DefaultApiInterface.md#ordersIdFeedbacksGet) | **GET** /orders/{id}/feedbacks/ | Отзывы заказчика и исполнителя к заказу
[**ordersIdGet**](DefaultApiInterface.md#ordersIdGet) | **GET** /orders/{id} | Получение подробного заказа заказчика
[**ordersIdMessagesDelete**](DefaultApiInterface.md#ordersIdMessagesDelete) | **DELETE** /orders/{id}/messages/ | Удалить сообщения, связанные с заказом
[**ordersIdMessagesGet**](DefaultApiInterface.md#ordersIdMessagesGet) | **GET** /orders/{id}/messages/ | Получение сообщений заказа
[**ordersIdMessagesPost**](DefaultApiInterface.md#ordersIdMessagesPost) | **POST** /orders/{id}/messages/ | Добавить сообщение к заказу
[**ordersIdPut**](DefaultApiInterface.md#ordersIdPut) | **PUT** /orders/{id} | Изменение заказа
[**ordersPost**](DefaultApiInterface.md#ordersPost) | **POST** /orders/ | Добавление заказа
[**respondsIdDelete**](DefaultApiInterface.md#respondsIdDelete) | **DELETE** /responds/{id} | Получить отклик по id
[**respondsIdGet**](DefaultApiInterface.md#respondsIdGet) | **GET** /responds/{id} | Получить отклик по id
[**respondsIdPut**](DefaultApiInterface.md#respondsIdPut) | **PUT** /responds/{id} | Изменить отклик по id
[**searchOffersGet**](DefaultApiInterface.md#searchOffersGet) | **GET** /search/offers | 
[**searchOrdersGet**](DefaultApiInterface.md#searchOrdersGet) | **GET** /search/orders | 
[**usersCustomersGet**](DefaultApiInterface.md#usersCustomersGet) | **GET** /users/customers/ | Получение всех пользователей-заказчиков
[**usersCustomersPost**](DefaultApiInterface.md#usersCustomersPost) | **POST** /users/customers/ | Добавление нового пользователя как заказчика
[**usersIdDelete**](DefaultApiInterface.md#usersIdDelete) | **DELETE** /users/{id} | Удаление пользователя
[**usersIdFeedbacksCustomerPost**](DefaultApiInterface.md#usersIdFeedbacksCustomerPost) | **POST** /users/{id}/feedbacks/customer/ | Добавить отзыв от лица заказчика
[**usersIdFeedbacksWorkerPost**](DefaultApiInterface.md#usersIdFeedbacksWorkerPost) | **POST** /users/{id}/feedbacks/worker/ | Добавить отзыв от лица исполнителя
[**usersIdGet**](DefaultApiInterface.md#usersIdGet) | **GET** /users/{id} | Получение пользователя
[**usersIdMessagesGet**](DefaultApiInterface.md#usersIdMessagesGet) | **GET** /users/{id}/messages/ | Получение сообщений пользователя
[**usersIdSettingSetAvatarPatch**](DefaultApiInterface.md#usersIdSettingSetAvatarPatch) | **PATCH** /users/{id}/setting/set/avatar | 
[**usersIdSettingSetEmailPatch**](DefaultApiInterface.md#usersIdSettingSetEmailPatch) | **PATCH** /users/{id}/setting/set/email | 
[**usersIdSettingSetNameSurnamePatch**](DefaultApiInterface.md#usersIdSettingSetNameSurnamePatch) | **PATCH** /users/{id}/setting/set/name_surname | 
[**usersIdSettingSetPasswordPatch**](DefaultApiInterface.md#usersIdSettingSetPasswordPatch) | **PATCH** /users/{id}/setting/set/password | 
[**usersIdSettingSetPhonePatch**](DefaultApiInterface.md#usersIdSettingSetPhonePatch) | **PATCH** /users/{id}/setting/set/phone | 


## Service Declaration
```yaml
# src/Acme/MyBundle/Resources/services.yml
services:
    # ...
    acme.my_bundle.api.:
        class: Acme\MyBundle\Api\Api
        tags:
            - { name: "swagger_server.api", api: "" }
    # ...
```

## **categoriesGet**
> Swagger\Server\Model\InlineResponse2003 categoriesGet()



Получение списка категорий заказов

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#categoriesGet
     */
    public function categoriesGet()
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Swagger\Server\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **citiesGet**
> Swagger\Server\Model\InlineResponse2002 citiesGet()



Получение списков городов

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#citiesGet
     */
    public function citiesGet()
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Swagger\Server\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **executorIdAddphotoPost**
> executorIdAddphotoPost($id, $photo)



### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#executorIdAddphotoPost
     */
    public function executorIdAddphotoPost($id, Photo $photo)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **photo** | [**Swagger\Server\Model\Photo**](../Model/Photo.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **executorIdDelete**
> executorIdDelete($id)



Удаление исполнителя

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#executorIdDelete
     */
    public function executorIdDelete($id)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **executorIdGet**
> Swagger\Server\Model\ExecFullItem executorIdGet($id)



Получение информации об исполнителе

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#executorIdGet
     */
    public function executorIdGet($id)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**Swagger\Server\Model\ExecFullItem**](../Model/ExecFullItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **executorIdOffersGet**
> Swagger\Server\Model\FoundedOffersList executorIdOffersGet($id)



Список предложений данного исполнителя

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#executorIdOffersGet
     */
    public function executorIdOffersGet($id)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**Swagger\Server\Model\FoundedOffersList**](../Model/FoundedOffersList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **executorIdOrdersGet**
> Swagger\Server\Model\FoundedOrdersList executorIdOrdersGet($id)



Заказы, связанные с данным исполнителем

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#executorIdOrdersGet
     */
    public function executorIdOrdersGet($id)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**Swagger\Server\Model\FoundedOrdersList**](../Model/FoundedOrdersList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **executorIdPut**
> executorIdPut($id, $executorRenewObject)



Обновление исполнителя

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#executorIdPut
     */
    public function executorIdPut($id, ExecFullItem $executorRenewObject)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **executorRenewObject** | [**Swagger\Server\Model\ExecFullItem**](../Model/ExecFullItem.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **executorIdSettingsEduPatch**
> Swagger\Server\Model\ExecFullItem executorIdSettingsEduPatch($id, $executorNewSkills)



Изменение информации об образовании исполнителя

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#executorIdSettingsEduPatch
     */
    public function executorIdSettingsEduPatch($id, ExecutorNewSkills $executorNewSkills)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **executorNewSkills** | [**Swagger\Server\Model\ExecutorNewSkills**](../Model/ExecutorNewSkills.md)|  |

### Return type

[**Swagger\Server\Model\ExecFullItem**](../Model/ExecFullItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **executorIdSettingsEmailPatch**
> Swagger\Server\Model\ExecFullItem executorIdSettingsEmailPatch($id, $executorNewMail)



Изменение e-mail исполнителя

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#executorIdSettingsEmailPatch
     */
    public function executorIdSettingsEmailPatch($id, ExecutorNewMail $executorNewMail)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **executorNewMail** | [**Swagger\Server\Model\ExecutorNewMail**](../Model/ExecutorNewMail.md)|  |

### Return type

[**Swagger\Server\Model\ExecFullItem**](../Model/ExecFullItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **executorIdSettingsNamePatch**
> Swagger\Server\Model\ExecFullItem executorIdSettingsNamePatch($id, $executorNewName)



Изменение имени исполнителя

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#executorIdSettingsNamePatch
     */
    public function executorIdSettingsNamePatch($id, ExecutorNewName $executorNewName)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **executorNewName** | [**Swagger\Server\Model\ExecutorNewName**](../Model/ExecutorNewName.md)|  |

### Return type

[**Swagger\Server\Model\ExecFullItem**](../Model/ExecFullItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **executorIdSettingsPasswdPatch**
> Swagger\Server\Model\ExecFullItem executorIdSettingsPasswdPatch($id, $executorNewPassword)



Изменение пароля исполнителя

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#executorIdSettingsPasswdPatch
     */
    public function executorIdSettingsPasswdPatch($id, ExecutorNewPassword $executorNewPassword)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **executorNewPassword** | [**Swagger\Server\Model\ExecutorNewPassword**](../Model/ExecutorNewPassword.md)|  |

### Return type

[**Swagger\Server\Model\ExecFullItem**](../Model/ExecFullItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **executorIdSettingsPhonePatch**
> Swagger\Server\Model\ExecFullItem executorIdSettingsPhonePatch($id, $executorNewPhone)



Изменение номера телефона исполнителя

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#executorIdSettingsPhonePatch
     */
    public function executorIdSettingsPhonePatch($id, ExecutorNewPhone $executorNewPhone)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **executorNewPhone** | [**Swagger\Server\Model\ExecutorNewPhone**](../Model/ExecutorNewPhone.md)|  |

### Return type

[**Swagger\Server\Model\ExecFullItem**](../Model/ExecFullItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **executorIdSettingsSkillsPatch**
> Swagger\Server\Model\ExecFullItem executorIdSettingsSkillsPatch($id, $executorNewSkills)



Изменение навыков исполнителя

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#executorIdSettingsSkillsPatch
     */
    public function executorIdSettingsSkillsPatch($id, array $executorNewSkills)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **executorNewSkills** | **string**|  |

### Return type

[**Swagger\Server\Model\ExecFullItem**](../Model/ExecFullItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **executorIdStatOrderstatGet**
> Swagger\Server\Model\InlineResponse2005 executorIdStatOrderstatGet($id)



### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#executorIdStatOrderstatGet
     */
    public function executorIdStatOrderstatGet($id)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**Swagger\Server\Model\InlineResponse2005**](../Model/InlineResponse2005.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **executorIdStatRatingstatGet**
> Swagger\Server\Model\InlineResponse2004 executorIdStatRatingstatGet($id)



### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#executorIdStatRatingstatGet
     */
    public function executorIdStatRatingstatGet($id)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**Swagger\Server\Model\InlineResponse2004**](../Model/InlineResponse2004.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **executorPost**
> executorPost($executorAdd)



Добавление исполнителя

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#executorPost
     */
    public function executorPost(ExecFullItem $executorAdd)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **executorAdd** | [**Swagger\Server\Model\ExecFullItem**](../Model/ExecFullItem.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **messagesIdGet**
> Swagger\Server\Model\ExtendedMessage messagesIdGet($id)

Получение сообщения по id

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#messagesIdGet
     */
    public function messagesIdGet($id)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id сообщения |

### Return type

[**Swagger\Server\Model\ExtendedMessage**](../Model/ExtendedMessage.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **offersIdDelete**
> offersIdDelete($id)



Удаление предложения по ID

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#offersIdDelete
     */
    public function offersIdDelete($id)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **offersIdGet**
> Swagger\Server\Model\OfferFullItem offersIdGet($id)



Получение предложения по ID

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#offersIdGet
     */
    public function offersIdGet($id)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**Swagger\Server\Model\OfferFullItem**](../Model/OfferFullItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **offersIdPut**
> offersIdPut($id, $offserChangeSettings)



Обновление предложения

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#offersIdPut
     */
    public function offersIdPut($id, OfferFullItem $offserChangeSettings)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **offserChangeSettings** | [**Swagger\Server\Model\OfferFullItem**](../Model/OfferFullItem.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **offersPost**
> Swagger\Server\Model\InlineResponse2001 offersPost($osserDetails)



Добавление нового предложения

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#offersPost
     */
    public function offersPost(OfferFullItem $osserDetails)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **osserDetails** | [**Swagger\Server\Model\OfferFullItem**](../Model/OfferFullItem.md)|  |

### Return type

[**Swagger\Server\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **orderIdRespondsGet**
> Swagger\Server\Model\ExtendedRespond orderIdRespondsGet($id)

Получить отклики по id заказа

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#orderIdRespondsGet
     */
    public function orderIdRespondsGet($id)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id заказа |

### Return type

[**Swagger\Server\Model\ExtendedRespond**](../Model/ExtendedRespond.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **orderIdRespondsPost**
> Swagger\Server\Model\ExtendedRespond orderIdRespondsPost($id, $respond)

Добавить отклик в заказ по id

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#orderIdRespondsPost
     */
    public function orderIdRespondsPost($id, BasicRespond $respond)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id заказа |
 **respond** | [**Swagger\Server\Model\BasicRespond**](../Model/BasicRespond.md)| id заказа |

### Return type

[**Swagger\Server\Model\ExtendedRespond**](../Model/ExtendedRespond.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **ordersGet**
> Swagger\Server\Model\ExtendedOrder ordersGet()

Получение списка заказов заказчика

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#ordersGet
     */
    public function ordersGet()
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Swagger\Server\Model\ExtendedOrder**](../Model/ExtendedOrder.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **ordersIdDelete**
> Swagger\Server\Model\ExtendedOrder ordersIdDelete($id)

Изменение заказа

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#ordersIdDelete
     */
    public function ordersIdDelete($id)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**Swagger\Server\Model\ExtendedOrder**](../Model/ExtendedOrder.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **ordersIdFeedbacksGet**
> Swagger\Server\Model\ExtendedFeedback ordersIdFeedbacksGet($id)

Отзывы заказчика и исполнителя к заказу

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#ordersIdFeedbacksGet
     */
    public function ordersIdFeedbacksGet($id)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id заказа |

### Return type

[**Swagger\Server\Model\ExtendedFeedback**](../Model/ExtendedFeedback.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **ordersIdGet**
> Swagger\Server\Model\ExtendedOrder ordersIdGet($id)

Получение подробного заказа заказчика

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#ordersIdGet
     */
    public function ordersIdGet($id)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**Swagger\Server\Model\ExtendedOrder**](../Model/ExtendedOrder.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **ordersIdMessagesDelete**
> ordersIdMessagesDelete($id)

Удалить сообщения, связанные с заказом

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#ordersIdMessagesDelete
     */
    public function ordersIdMessagesDelete($id)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id заказа |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **ordersIdMessagesGet**
> Swagger\Server\Model\ExtendedMessage ordersIdMessagesGet($id)

Получение сообщений заказа

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#ordersIdMessagesGet
     */
    public function ordersIdMessagesGet($id)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id заказа |

### Return type

[**Swagger\Server\Model\ExtendedMessage**](../Model/ExtendedMessage.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **ordersIdMessagesPost**
> Swagger\Server\Model\ExtendedMessage ordersIdMessagesPost($id)

Добавить сообщение к заказу

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#ordersIdMessagesPost
     */
    public function ordersIdMessagesPost($id)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id заказа |

### Return type

[**Swagger\Server\Model\ExtendedMessage**](../Model/ExtendedMessage.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **ordersIdPut**
> Swagger\Server\Model\ExtendedOrder ordersIdPut($id, $order)

Изменение заказа

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#ordersIdPut
     */
    public function ordersIdPut($id, BasicOrder $order)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **order** | [**Swagger\Server\Model\BasicOrder**](../Model/BasicOrder.md)|  |

### Return type

[**Swagger\Server\Model\ExtendedOrder**](../Model/ExtendedOrder.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **ordersPost**
> Swagger\Server\Model\ExtendedOrder ordersPost($order)

Добавление заказа

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#ordersPost
     */
    public function ordersPost(BasicOrder $order)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order** | [**Swagger\Server\Model\BasicOrder**](../Model/BasicOrder.md)|  |

### Return type

[**Swagger\Server\Model\ExtendedOrder**](../Model/ExtendedOrder.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **respondsIdDelete**
> respondsIdDelete($id)

Получить отклик по id

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#respondsIdDelete
     */
    public function respondsIdDelete($id)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id отклика |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **respondsIdGet**
> Swagger\Server\Model\ExtendedRespond respondsIdGet($id)

Получить отклик по id

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#respondsIdGet
     */
    public function respondsIdGet($id)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id отклика |

### Return type

[**Swagger\Server\Model\ExtendedRespond**](../Model/ExtendedRespond.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **respondsIdPut**
> Swagger\Server\Model\ExtendedRespond respondsIdPut($id, $respond)

Изменить отклик по id

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#respondsIdPut
     */
    public function respondsIdPut($id, BasicRespond $respond)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id отклика |
 **respond** | [**Swagger\Server\Model\BasicRespond**](../Model/BasicRespond.md)| Изменённый объект отклика |

### Return type

[**Swagger\Server\Model\ExtendedRespond**](../Model/ExtendedRespond.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **searchOffersGet**
> Swagger\Server\Model\FoundedOffersList searchOffersGet($query, $category, $minCost, $maxCost, $dueTime)



### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#searchOffersGet
     */
    public function searchOffersGet($query, $category = null, $minCost = null, $maxCost = null, $dueTime = null)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | **string**| Строка поискового запроса |
 **category** | **int**| Категория | [optional]
 **minCost** | **string**| Минимальная плата | [optional]
 **maxCost** | **string**| Максимальная плата | [optional]
 **dueTime** | **string**| Максимальный срок исполнения (часов) | [optional]

### Return type

[**Swagger\Server\Model\FoundedOffersList**](../Model/FoundedOffersList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **searchOrdersGet**
> Swagger\Server\Model\FoundedOrdersList searchOrdersGet($query, $category, $minCost, $maxCost, $dueTime)



### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#searchOrdersGet
     */
    public function searchOrdersGet($query, $category = null, $minCost = null, $maxCost = null, $dueTime = null)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | **string**| Строка поискового запроса |
 **category** | **int**| Категория | [optional]
 **minCost** | **string**| Минимальная плата | [optional]
 **maxCost** | **string**| Максимальная плата | [optional]
 **dueTime** | **string**| Максимальный срок исполнения (часов) | [optional]

### Return type

[**Swagger\Server\Model\FoundedOrdersList**](../Model/FoundedOrdersList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **usersCustomersGet**
> Swagger\Server\Model\ExtendedOrder usersCustomersGet()

Получение всех пользователей-заказчиков

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#usersCustomersGet
     */
    public function usersCustomersGet()
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Swagger\Server\Model\ExtendedOrder**](../Model/ExtendedOrder.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **usersCustomersPost**
> Swagger\Server\Model\ExtendedCustomer usersCustomersPost($order)

Добавление нового пользователя как заказчика

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#usersCustomersPost
     */
    public function usersCustomersPost(Order $order)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order** | [**Swagger\Server\Model\Order**](../Model/Order.md)|  |

### Return type

[**Swagger\Server\Model\ExtendedCustomer**](../Model/ExtendedCustomer.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **usersIdDelete**
> usersIdDelete($id)

Удаление пользователя

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#usersIdDelete
     */
    public function usersIdDelete($id)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **usersIdFeedbacksCustomerPost**
> Swagger\Server\Model\ExtendedFeedback usersIdFeedbacksCustomerPost($id, $feedback)

Добавить отзыв от лица заказчика

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#usersIdFeedbacksCustomerPost
     */
    public function usersIdFeedbacksCustomerPost($id, BasicFeedback $feedback)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id заказа |
 **feedback** | [**Swagger\Server\Model\BasicFeedback**](../Model/BasicFeedback.md)| Новый объект отзыва |

### Return type

[**Swagger\Server\Model\ExtendedFeedback**](../Model/ExtendedFeedback.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **usersIdFeedbacksWorkerPost**
> Swagger\Server\Model\ExtendedFeedback usersIdFeedbacksWorkerPost($id, $feedback)

Добавить отзыв от лица исполнителя

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#usersIdFeedbacksWorkerPost
     */
    public function usersIdFeedbacksWorkerPost($id, BasicFeedback $feedback)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id заказа |
 **feedback** | [**Swagger\Server\Model\BasicFeedback**](../Model/BasicFeedback.md)| Новый объект отзыва |

### Return type

[**Swagger\Server\Model\ExtendedFeedback**](../Model/ExtendedFeedback.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **usersIdGet**
> Swagger\Server\Model\ExtendedCustomer usersIdGet($id)

Получение пользователя

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#usersIdGet
     */
    public function usersIdGet($id)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**Swagger\Server\Model\ExtendedCustomer**](../Model/ExtendedCustomer.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **usersIdMessagesGet**
> usersIdMessagesGet($id)

Получение сообщений пользователя

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#usersIdMessagesGet
     */
    public function usersIdMessagesGet($id)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id заказа |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **usersIdSettingSetAvatarPatch**
> usersIdSettingSetAvatarPatch($id, $avatarImg)



Изменение пароля

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#usersIdSettingSetAvatarPatch
     */
    public function usersIdSettingSetAvatarPatch($id, AvatarImg $avatarImg)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **avatarImg** | [**Swagger\Server\Model\AvatarImg**](../Model/AvatarImg.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **usersIdSettingSetEmailPatch**
> usersIdSettingSetEmailPatch($id, $password)



Изменение email

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#usersIdSettingSetEmailPatch
     */
    public function usersIdSettingSetEmailPatch($id, Password1 $password)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **password** | [**Swagger\Server\Model\Password1**](../Model/Password1.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **usersIdSettingSetNameSurnamePatch**
> usersIdSettingSetNameSurnamePatch($id, $nameSurname)



Изменение имени и фамилии

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#usersIdSettingSetNameSurnamePatch
     */
    public function usersIdSettingSetNameSurnamePatch($id, NameSurname $nameSurname)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **nameSurname** | [**Swagger\Server\Model\NameSurname**](../Model/NameSurname.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **usersIdSettingSetPasswordPatch**
> usersIdSettingSetPasswordPatch($id, $password)



Изменение пароля

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#usersIdSettingSetPasswordPatch
     */
    public function usersIdSettingSetPasswordPatch($id, Password $password)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **password** | [**Swagger\Server\Model\Password**](../Model/Password.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **usersIdSettingSetPhonePatch**
> usersIdSettingSetPhonePatch($id, $phone)



Изменение номера телефона

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class Api implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#usersIdSettingSetPhonePatch
     */
    public function usersIdSettingSetPhonePatch($id, Phone $phone)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **phone** | [**Swagger\Server\Model\Phone**](../Model/Phone.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

