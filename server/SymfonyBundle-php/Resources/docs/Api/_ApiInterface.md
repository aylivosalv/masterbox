# Swagger\Server\Api\_ApiInterface

All URIs are relative to *https://virtserver.swaggerhub.com/aylivosalv/MasterBox/0.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**loginPost**](_ApiInterface.md#loginPost) | **POST** /login | 
[**registerPost**](_ApiInterface.md#registerPost) | **POST** /register | 


## Service Declaration
```yaml
# src/Acme/MyBundle/Resources/services.yml
services:
    # ...
    acme.my_bundle.api._:
        class: Acme\MyBundle\Api\_Api
        tags:
            - { name: "swagger_server.api", api: "_" }
    # ...
```

## **loginPost**
> Swagger\Server\Model\InlineResponse200 loginPost($login)



### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/_ApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\_ApiInterface;

class _Api implements _ApiInterface
{

    // ...

    /**
     * Implementation of _ApiInterface#loginPost
     */
    public function loginPost(Login $login)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **login** | [**Swagger\Server\Model\Login**](../Model/Login.md)|  |

### Return type

[**Swagger\Server\Model\InlineResponse200**](../Model/InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **registerPost**
> Swagger\Server\Model\InlineResponse200 registerPost($reg)



### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/_ApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\_ApiInterface;

class _Api implements _ApiInterface
{

    // ...

    /**
     * Implementation of _ApiInterface#registerPost
     */
    public function registerPost(Reg $reg)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reg** | [**Swagger\Server\Model\Reg**](../Model/Reg.md)|  |

### Return type

[**Swagger\Server\Model\InlineResponse200**](../Model/InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

