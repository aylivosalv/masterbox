<?php
/**
 * InlineResponse2001Test
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Server\Tests\Model
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * MasterBox - Freelance exchange
 *
 * Freelance exchange service
 *
 * OpenAPI spec version: 0.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Server\Model;

/**
 * InlineResponse2001Test Class Doc Comment
 *
 * @category    Class */
// * @description InlineResponse2001
/**
 * @package     Swagger\Server\Tests\Model
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class InlineResponse2001Test extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "InlineResponse2001"
     */
    public function testInlineResponse2001()
    {
        $testInlineResponse2001 = new InlineResponse2001();
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }
}
