<?php
/**
 * Order
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Server\Model
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * MasterBox - Freelance exchange
 *
 * Freelance exchange service
 *
 * OpenAPI spec version: 0.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Server\Model;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;

/**
 * Class representing the Order model.
 *
 * @package Swagger\Server\Model
 * @author  Swagger Codegen team
 */
class Order 
{
        /**
     * В случае успешного ответа
     *
     * @var bool|null
     * @SerializedName("success")
     * @Assert\Type("bool")
     * @Type("bool")
     */
    protected $success;

    /**
     * @var Swagger\Server\Model\BasicCustomer|null
     * @SerializedName("data")
     * @Assert\Type("Swagger\Server\Model\BasicCustomer")
     * @Type("Swagger\Server\Model\BasicCustomer")
     */
    protected $data;

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->success = isset($data['success']) ? $data['success'] : null;
        $this->data = isset($data['data']) ? $data['data'] : null;
    }

    /**
     * Gets success.
     *
     * @return bool|null
     */
    public function isSuccess()
    {
        return $this->success;
    }

    /**
     * Sets success.
     *
     * @param bool|null $success  В случае успешного ответа
     *
     * @return $this
     */
    public function setSuccess($success = null)
    {
        $this->success = $success;

        return $this;
    }

    /**
     * Gets data.
     *
     * @return Swagger\Server\Model\BasicCustomer|null
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Sets data.
     *
     * @param Swagger\Server\Model\BasicCustomer|null $data
     *
     * @return $this
     */
    public function setData(BasicCustomer $data = null)
    {
        $this->data = $data;

        return $this;
    }
}


